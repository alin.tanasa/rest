﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiLayer.Models
{
	public class Books
	{
		public Guid Id { get; set; }

		public LibraryResponseModel library { get; set; }
	}
}
