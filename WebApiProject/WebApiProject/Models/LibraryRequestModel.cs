﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApiLayer.Models
{
    public class LibraryRequestModel
    {
        [Required][MaxLength(40)]
        public string Name { get; set; }
        [Required][EmailAddress]
        public string EmailAddress { get; set; }
        public string PhysicalAddress { get; set; }
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
