﻿using System;
using ApiLayer.Models;
using BusinessLayer.Models;

namespace ApiLayer
{
    public static class Mapper
    {
        public static LibraryResponseModel MapToResponseModel(LibraryReadModel readModel)
        {
            return new LibraryResponseModel
            {
                Id = readModel.Id,
                EmailAddress = readModel.EmailAddress,
                Name = readModel.Name,
                PhoneNumber = readModel.PhoneNumber,
                PhysicalAddress = readModel.PhysicalAddress
            };
        }

        public static LibraryWriteModel MapToWriteModel(LibraryRequestModel requestModel)
        {
            return new BusinessLayer.Models.LibraryWriteModel
            {
                Name = requestModel.Name,
                EmailAddress = requestModel.EmailAddress,
                PhoneNumber = requestModel.PhoneNumber,
                PhysicalAddress = requestModel.PhysicalAddress
            };
        }
    }
}
